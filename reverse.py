from cfn_tools import load_yaml
from troposphere import TemplateGenerator
template = TemplateGenerator(load_yaml(
            app_config.cloudformation.meta.client.get_template(
                StackName='MyStack')['TemplateBody']
        ))